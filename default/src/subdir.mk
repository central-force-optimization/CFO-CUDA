################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/hr_time.cpp 

CU_SRCS += \
../src/main.cu 

CU_DEPS += \
./src/cu_main.d 

OBJS += \
./src/hr_time.o \
./src/cu_main.o 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: C++ Compiler'
	g++ -O2 -g -Wall -c -fmessage-length=0 -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/cu_%.o: ../src/%.cu
	@echo 'Building file: $<'
	@echo 'Invoking: CUDA NVCC Compiler'
	nvcc -c -I/usr/local/cuda/include -o "$@" "$<" && \
	echo -n '$(@:%.o=%.d)' $(dir $@) > '$(@:%.o=%.d)' && \
	nvcc -M -I/usr/local/cuda/include   "$<" >> '$(@:%.o=%.d)'
	@echo 'Finished building: $<'
	@echo ' '


